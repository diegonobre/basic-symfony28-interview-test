<?php

/**
 * Service to manipulate Bios documents
 *
 * @since 2016-08-09
 * @author Diego Nobre <dcnobre@gmail.com>
 */

namespace Test\InterviewBundle\Service;

class BiosService
{
    /**
     * Return all awards
     *
     * @return mixed
     */
    public function getAllAwards()
    {
        $documents = $this->get('doctrine_mongodb')
            ->getRepository('InterviewBundle:Bios')
            ->findAll();

        if (!$documents) {
            throw $this->createNotFoundException('No document found on database.');
        }

        return $documents;
    }

    /**
     * Return documents by contribution name
     *
     * @param $contributionName
     * @return mixed
     */
    public function getBiosByContribution($contributionName)
    {
        $documents = $this->get('doctrine_mongodb')
            ->getRepository('InterviewBundle:Bios')
            ->findByContribution($contributionName);

        if (!$documents) {
            throw $this->createNotFoundException('No document found for contributionName '.$contributionName);
        }

        return $documents;
    }

    /**
     * Return true if a document with specified id exists
     *
     * @param $id
     * @return bool
     */
    public function hasDocumentById($id)
    {
        $document = $this->get('doctrine_mongodb')
            ->getRepository('InterviewBundle:Bios')
            ->find($id);

        if (!$document) {
            return false;
        }

        return true;
    }
}
