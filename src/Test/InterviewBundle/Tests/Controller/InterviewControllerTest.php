<?php

namespace Test\InterviewBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InterviewControllerTest extends WebTestCase
{
    public function testContributions()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/fake');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        /*
        $client->request('GET', '/OOP');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        */
    }
}
