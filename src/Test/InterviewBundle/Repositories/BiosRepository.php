<?php

/**
 * Repository to manipulate Bios documents
 *
 * @since 2016-08-09
 * @author Diego Nobre <dcnobre@gmail.com>
 */

namespace Test\InterviewBundle\Repositories;

use Doctrine\ODM\MongoDB\DocumentRepository;

class BiosRepository extends DocumentRepository
{
    /**
     * Find documents by first name
     *
     * @param $firstName
     * @return mixed
     */
    public function findByFirstName($firstName)
    {
        return $this->createQueryBuilder()
            ->field('name.$first')->equals($firstName)
            ->getQuery()
            ->execute();
    }

    /**
     * Find by awards contribution name
     *
     * @param $contributionName
     * @return mixed
     */
    public function findByContribution($contributionName)
    {
        return $this->createQueryBuilder()
            ->field('awards.award.$by')->equals($contributionName)
            ->getQuery()
            ->execute();
    }

    /**
     * Find by dead before specific year
     *
     * @param $year
     * @return mixed
     */
    public function findByDeadBefore($year)
    {
        return $this->createQueryBuilder()
            ->field('birth')->lt($year)
            ->getQuery()
            ->execute();
    }
}
