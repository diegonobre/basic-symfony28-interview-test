<?php

namespace Test\InterviewBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContributionsController extends Controller
{
    /**
     * @Route("/contributions", name="contributions")
     */
    public function contributionsAction()
    {
        $biosService = $this->get('interview.bios');

        return new JsonResponse($biosService->getAllAwards());
    }

    /**
     * @Route("/contributions/{contributionName}", name="bios_by_contribution")
     */
    public function biosByContributionAction($contributionName)
    {
        $biosService = $this->get('interview.bios');
        $biosByContribution = $biosService->getBiosByContribution($contributionName);

        if (!$biosByContribution) {
            throw $this->createNotFoundException('Documents with this contribution name does not exist');
        }

        return new JsonResponse($biosByContribution);
    }
}
