<?php

namespace Test\InterviewBundle\Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(
 *     collection="Bios",
 *     repositoryClass="InterviewBundle\Repositories\BiosRepository"
 * )
 */
class Bios
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\EmbedOne(targetDocument="Name")
     */
    protected $name = array();

    /**
     * @MongoDB\Field(type="date")
     */
    protected $birth;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $death;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $contribs = array();

    /**
     * @MongoDB\EmbedMany(targetDocument="Award")
     */
    protected $awards;
}

/**
 * @MongoDB\EmbeddedDocument
 */
class Name
{
    /**
     * @MongoDB\Field(type="string")
     */
    public $first;

    /**
     * @MongoDB\Field(type="string")
     */
    public $last;
}

/**
 * @MongoDB\EmbeddedDocument
 */
class Awards
{
    /**
     * @MongoDB\Field(type="string")
     */
    public $award;

    /**
     * @MongoDB\Field(type="integer")
     */
    public $year;

    /**
     * @MongoDB\Field(type="string")
     */
    public $by;
}
