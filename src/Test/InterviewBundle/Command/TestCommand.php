<?php

/**
 * Simple command to return if a Bios document exists
 *
 * @since 2016-08-09
 * @author Diego Nobre <dcnobre@gmail.com>
 */

namespace Test\InterviewBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected function configure()
    {
        $this->setName('test:command')
            ->setDescription('Inform if a document exists')
            ->setHelp("One id as argument should inform if a document exists")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Document exists?',
            '================',
            'Trying to find document with id: '.$input,
        ]);

        $biosService = $this->get('interview.bios');

        if (!$biosService->hasDocumentById($input)) {
            $output->writeln('document doesnt exist');
        }

        $output->writeln('document exists');
    }
}
