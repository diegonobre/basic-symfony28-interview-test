<?php

/**
 * Bundle for test using Symfony 2.8 + Doctrine + MongoDB
 *
 * @since 2016-08-09
 * @author Diego Nobre <dcnobre@gmail.com>
 */

namespace Test\InterviewBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class InterviewBundle extends Bundle
{
}
